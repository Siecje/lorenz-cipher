from lorenz.lorenz import string_to_baudot, lorenz, baudot_to_string

starting_chi_positions = [0, 0, 0, 0, 0]
starting_mu_positions = [0, 0]
starting_psi_positions = [0, 0, 0, 0, 0]

plaintext = """
It is a truth universally acknowledged, that a single man in possession of a good
fortune, must be in want of a wife.
However little known the feelings or views of such a man may be on his first
entering a neighbourhood, this truth is so well fixed in the minds of the sur-
rounding families, that he is considered the rightful property of some one or
other of their daughters.
"My dear Mr. Bennet," said his lady to him one day, "have you heard that
Netherfield Park is let at last?"
Mr. Bennet replied that he had not.
"""

#plaintext = 'cody19scott'
#plaintext = '''CBJLP
#S
#BQL>$
#'''

plaintext = plaintext.replace('<', '').replace('>', '')

print 'Plaintext: ', plaintext

#convert to baudot
baudot_plaintext = string_to_baudot(plaintext)

#Convert back to characters
back_to_plain = baudot_to_string(baudot_plaintext)

# Remove switch characters
back_to_string = ''.join(back_to_plain).replace('<', '').replace('>', '')
print 'Converted to baudot and back', back_to_string
assert(back_to_string == plaintext.upper())

print back_to_string

print plaintext.upper()

#Convert back to baudot
back_to_baudot = string_to_baudot(back_to_string)
assert(back_to_baudot == baudot_plaintext)


baudot_cipher_text = lorenz(
    baudot_plaintext,
    starting_chi_positions,
    starting_mu_positions,
    starting_psi_positions
)

# Convert baudot to characters
cipher_text = baudot_to_string(baudot_cipher_text)

cipher_string = ''.join(cipher_text)
print 'Cipher Characters', cipher_string

#Convert Back
back_to_baudot_cipher = string_to_baudot(cipher_string)
assert(back_to_baudot_cipher == baudot_cipher_text)

# Decrypt should get original message

# With switch characters
plaintext2 = cipher_string
#convert to baudot
baudot_plaintext2 = string_to_baudot(plaintext2)


baudot_cipher_text2 = lorenz(
    baudot_plaintext2,
    starting_chi_positions,
    starting_mu_positions,
    starting_psi_positions
)

#Convert to characters
cipher_text2 = baudot_to_string(baudot_cipher_text2)
#Remove Swtich characters
cipher_string = ''.join(cipher_text2).replace('<', '').replace('>', '')

print cipher_string

