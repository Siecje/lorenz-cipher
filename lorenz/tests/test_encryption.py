from lorenz.lorenz import xor, wheel_list_encrypt

assert(xor(0, 0) == 0)
assert(xor(0, 1) == 1)
assert(xor(1, 0) == 1)
assert(xor(1, 1) == 0)

wheel_list = [
    [1, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
baudot_code = (1, 0, 1, 0, 1)

result = wheel_list_encrypt(baudot_code, wheel_list)
assert(result == (0, 0, 0, 1, 1))


wheel_list2 = [
    [1, 0, 0, 1, 1],
    [1, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [1, 0, 0, 1, 0],
]
baudot_code2 = (1, 1, 1, 1, 1)

result2 = wheel_list_encrypt(baudot_code2, wheel_list2)
assert(result2 == (0, 0, 0, 0, 0))

wheel_list3 = [
    [0, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [0, 0, 1, 1, 1],
    [0, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
baudot_code3 = (0, 0, 0, 0, 0)

result3 = wheel_list_encrypt(baudot_code3, wheel_list3)
assert(result3 == (0, 0, 0, 0, 0))

wheel_list4 = [
    [1, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [0, 1, 0, 1, 1],
    [1, 0, 0, 1, 0],
]
baudot_code4 = (0, 1, 0, 1, 0)

result4 = wheel_list_encrypt(baudot_code4, wheel_list4)
assert(result4 == (1, 1, 1, 1, 1))


