from ..lorenz.lorenz import baudot_to_string, string_to_baudot

def convert_and_reverse(text, baudot=False):
    if baudot is False:
        baudot_codes = string_to_baudot(text)
        back_to_text = baudot_to_string(baudot_codes)
        back_to_string = ''.join(back_to_text)
        assert(back_to_string == text.upper())
    elif baudot is True:
        to_text = baudot_to_string(text)
        back_to_baudot = string_to_baudot(to_text)
        assert(back_to_baudot == text)
        text_string = ''.join(to_text)
        back_to_baudot2 = string_to_baudot(text_string)
        assert(back_to_baudot2 == text)


letters = 'The quick brown fox jumped over the slow lazy dog.\n'
convert_and_reverse(letters, baudot=False)

figures = '!"#&\'(),./0123456789:;?\n -'
convert_and_reverse(figures, baudot=False)

letters_and_figures = 'words 12 numbers :!? both'
convert_and_reverse(letters_and_figures, baudot=False)

baudot_of_letters = string_to_baudot(letters)
convert_and_reverse(baudot_of_letters, baudot=True)

baudot_of_figures = string_to_baudot(figures)
convert_and_reverse(baudot_of_figures, baudot=True)

baudot_of_both = string_to_baudot(letters_and_figures)
convert_and_reverse(baudot_of_both, baudot=True)
