from lorenz.lorenz import rotate_wheel, rotate_wheel_by, rotate_wheel_list, rotate_first_wheel_in_list, rotate_second_wheel_in_list, set_wheel_list_positions


wheel = [1, 0, 0, 1, 1]
wheel = rotate_wheel(wheel)
assert(wheel == [0, 0, 1, 1, 1])

wheel1 = [0, 0, 0, 0, 0, 0, 0]
wheel1 = rotate_wheel(wheel1)
assert(wheel1 == [0, 0, 0, 0, 0, 0, 0])

wheel2 = [1, 1, 1, 1, 1, 1, 1, 1]
wheel2 = rotate_wheel(wheel2)
assert(wheel2 == [1, 1, 1, 1, 1, 1, 1, 1])

wheel3 = [0, 1, 0, 0, 1]
wheel3 = rotate_wheel_by(wheel3, 4)
assert(wheel3 == [1, 0, 1, 0, 0])

wheel4 = [0, 1, 0, 0, 1]
wheel4 = rotate_wheel_by(wheel4, 5)
assert(wheel4 == [0, 1, 0, 0, 1])

wheel4 = [0, 1, 0, 0, 1]
wheel4 = rotate_wheel_by(wheel4, 0)
assert(wheel4 == [0, 1, 0, 0, 1])

wheel_list = [
    [1, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
wheel_list = rotate_wheel_list(wheel_list)
rotated_wheel_list = [
    [0, 0, 1, 1, 1],
    [1, 0, 0, 0, 0],
    [0, 1, 1, 1, 1],
    [1, 0, 1, 1, 1],
    [0, 0, 1, 0, 0],
]
assert(wheel_list == rotated_wheel_list)


wheel_list2 = [
    [1, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
wheel_list2 = rotate_first_wheel_in_list(wheel_list2)
first_wheel_rotated = [
    [0, 0, 1, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
assert(wheel_list2 == first_wheel_rotated)

wheel_list3 = [
    [1, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
wheel_list3 = rotate_second_wheel_in_list(wheel_list3)
second_wheel_rotated = [
    [1, 0, 0, 1, 1],
    [1, 0, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
assert(wheel_list3 == second_wheel_rotated)


# Test starting positions
wheel_list4 = [
    [1, 0, 0, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 0, 1, 1, 1],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 1, 0],
]
starting_positions = [1, 0, 2, 3, 5]
wheel_list4 = set_wheel_list_positions(wheel_list4, starting_positions)
starting_wheel_list = [
    [0, 0, 1, 1, 1],
    [0, 1, 0, 0, 0],
    [1, 1, 1, 1, 0],
    [1, 1, 1, 1, 0],
    [0, 0, 0, 1, 0],
]
assert(wheel_list4 == starting_wheel_list)


