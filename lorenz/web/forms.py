from flask_wtf import Form
from wtforms import TextAreaField, SelectField, validators


chi_1_choices = [(i, i) for i in range(23)]
chi_2_choices = [(i, i) for i in range(26)]
chi_3_choices = [(i, i) for i in range(29)]
chi_4_choices = [(i, i) for i in range(31)]
chi_5_choices = [(i, i) for i in range(41)]

mu_1_choices = [(i, i) for i in range(61)]
mu_2_choices = [(i, i) for i in range(37)]

psi_1_choices = [(i, i) for i in range(59)]
psi_2_choices = [(i, i) for i in range(53)]
psi_3_choices = [(i, i) for i in range(51)]
psi_4_choices = [(i, i) for i in range(47)]
psi_5_choices = [(i, i) for i in range(43)]


class EncryptForm(Form):
    message = TextAreaField('message', [validators.Required()])

    # Starting Positions for chi wheels
    chi_1 = SelectField('chi_1', coerce=int, choices=chi_1_choices)
    chi_2 = SelectField('chi_2', coerce=int, choices=chi_2_choices)
    chi_3 = SelectField('chi_3', coerce=int, choices=chi_3_choices)
    chi_4 = SelectField('chi_4', coerce=int, choices=chi_4_choices)
    chi_5 = SelectField('chi_5', coerce=int, choices=chi_5_choices)

    # Starting Positions for mu wheels
    mu_1 = SelectField('mu_1', coerce=int, choices=mu_1_choices)
    mu_2 = SelectField('mu_2', coerce=int, choices=mu_2_choices)

    # Starting Positions for psi wheels
    psi_1 = SelectField('psi_1', coerce=int, choices=psi_1_choices)
    psi_2 = SelectField('psi_2', coerce=int, choices=psi_2_choices)
    psi_3 = SelectField('psi_3', coerce=int, choices=psi_3_choices)
    psi_4 = SelectField('psi_4', coerce=int, choices=psi_4_choices)
    psi_5 = SelectField('psi_5', coerce=int, choices=psi_5_choices)