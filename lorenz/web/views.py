import os
from flask import Flask, flash, render_template, request, redirect, url_for
from .forms import EncryptForm
from lorenz.lorenz import string_to_baudot, baudot_to_string, lorenz


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)
#app.debug = True
app.secret_key = 'math-rocks!'

previous_message = ''

@app.route('/', methods=['POST', 'GET'])
def home():
    global previous_message
    #TODO: show ciphertext as you type
    form = EncryptForm()
    if form.validate_on_submit():
        message = str(form.message.data)

        message = message.replace('\r\n', '\n')

        chi_1 = form.chi_1.data
        chi_2 = form.chi_1.data
        chi_3 = form.chi_1.data
        chi_4 = form.chi_1.data
        chi_5 = form.chi_1.data

        mu_1 = form.mu_1.data
        mu_2 = form.mu_2.data

        psi_1 = form.psi_1.data
        psi_2 = form.psi_2.data
        psi_3 = form.psi_3.data
        psi_4 = form.psi_4.data
        psi_5 = form.psi_5.data

        baudot_plaintext = string_to_baudot(message)

        starting_chi_positions = [chi_1, chi_2, chi_3, chi_4, chi_5]
        starting_mu_positions = [mu_1, mu_2]
        starting_psi_positions = [psi_1, psi_2, psi_3, psi_4, psi_5]
        print starting_chi_positions
        print starting_mu_positions
        print starting_psi_positions
        baudot_cipher_text = lorenz(
            baudot_plaintext,
            starting_chi_positions,
            starting_mu_positions,
            starting_psi_positions
        )
        # Cipher Text
        cipher_text = baudot_to_string(baudot_cipher_text)
        previous_message = ''.join(cipher_text)
        print previous_message
        return redirect(url_for('show_encrypted', message=''.join(cipher_text)))
    form.message.data = previous_message
    return render_template('home.html', form=form)


@app.route('/encrypted', methods=['GET'])
def show_encrypted(*args, **kwargs):
    #message = kwargs.get('message')
    #TODO: show baudot code?
    message = request.args.get('message')
    message = message.replace('<', '').replace('>', '')
    return render_template('show_encrypted.html', message=message)
