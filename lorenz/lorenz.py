def xor(a, b):
    return int(bool(a) != bool(b))


LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
FIGURES = '!"#&\'(),./0123456789:;?-$'


baudot_to_letter = {
    (1, 1, 0, 0, 0): 'A',
    (1, 0, 0, 1, 1): 'B',
    (0, 1, 1, 1, 0): 'C',
    (1, 0, 0, 1, 0): 'D',
    (1, 0, 0, 0, 0): 'E',
    (1, 0, 1, 1, 0): 'F',
    (0, 1, 0, 1, 1): 'G',
    (0, 0, 1, 0, 1): 'H',
    (0, 1, 1, 0, 0): 'I',
    (1, 1, 0, 1, 0): 'J',
    (1, 1, 1, 1, 0): 'K',
    (0, 1, 0, 0, 1): 'L',
    (0, 0, 1, 1, 1): 'M',
    (0, 0, 1, 1, 0): 'N',
    (0, 0, 0, 1, 1): 'O',
    (0, 1, 1, 0, 1): 'P',
    (1, 1, 1, 0, 1): 'Q',
    (0, 1, 0, 1, 0): 'R',
    (1, 0, 1, 0, 0): 'S',
    (0, 0, 0, 0, 1): 'T',
    (1, 1, 1, 0, 0): 'U',
    (0, 1, 1, 1, 1): 'V',
    (1, 1, 0, 0, 1): 'W',
    (1, 0, 1, 1, 1): 'X',
    (1, 0, 1, 0, 1): 'Y',
    (1, 0, 0, 0, 1): 'Z',
    #Switch to Letters
    (1, 1, 1, 1, 1): '<',
    #Switch to Figures
    (1, 1, 0, 1, 1): '>',
    #Carriage Return
    (0, 0, 0, 1, 0): '\n',
    #Line Feed
    #'\n': (0, 1, 0, 0, 0),
    (0, 1, 0, 0, 0): '_',
    (0, 0, 1, 0, 0): ' ',
    (0, 0, 0, 0, 0): '*',
}


letter_to_baudot = {
    'A': (1, 1, 0, 0, 0),
    'B': (1, 0, 0, 1, 1),
    'C': (0, 1, 1, 1, 0),
    'D': (1, 0, 0, 1, 0),
    'E': (1, 0, 0, 0, 0),
    'F': (1, 0, 1, 1, 0),
    'G': (0, 1, 0, 1, 1),
    'H': (0, 0, 1, 0, 1),
    'I': (0, 1, 1, 0, 0),
    'J': (1, 1, 0, 1, 0),
    'K': (1, 1, 1, 1, 0),
    'L': (0, 1, 0, 0, 1),
    'M': (0, 0, 1, 1, 1),
    'N': (0, 0, 1, 1, 0),
    'O': (0, 0, 0, 1, 1),
    'P': (0, 1, 1, 0, 1),
    'Q': (1, 1, 1, 0, 1),
    'R': (0, 1, 0, 1, 0),
    'S': (1, 0, 1, 0, 0),
    'T': (0, 0, 0, 0, 1),
    'U': (1, 1, 1, 0, 0),
    'V': (0, 1, 1, 1, 1),
    'W': (1, 1, 0, 0, 1),
    'X': (1, 0, 1, 1, 1),
    'Y': (1, 0, 1, 0, 1),
    'Z': (1, 0, 0, 0, 1),
    #Switch to Letters
    '<': (1, 1, 1, 1, 1),
    #Switch to Figures
    '>': (1, 1, 0, 1, 1),
    #Carriage Return
    '\n': (0, 0, 0, 1, 0),
    #Line Feed
    #'\n': (0, 1, 0, 0, 0),
    '_': (0, 1, 0, 0, 0),
    ' ': (0, 0, 1, 0, 0),
    '*': (0, 0, 0, 0, 0),
}

figures_to_baudot = {
    '!': (1, 0, 1, 1, 0),
    '"': (1, 0, 0, 0, 1),
    '#': (0, 0, 1, 0, 1),
    '&': (0, 1, 0, 1, 1),
    "'": (1, 1, 0, 1, 0),
    '(': (1, 1, 1, 1, 0),
    ')': (0, 1, 0, 0, 1),
    ',': (0, 0, 1, 1, 0),
    '.': (0, 0, 1, 1, 1),
    '/': (1, 0, 1, 1, 1),
    '0': (0, 1, 1, 0, 1),
    '1': (1, 1, 1, 0, 1),
    '2': (1, 1, 0, 0, 1),
    '3': (1, 0, 0, 0, 0),
    '4': (0, 1, 0, 1, 0),
    '5': (0, 0, 0, 0, 1),
    '6': (1, 0, 1, 0, 1),
    '7': (1, 1, 1, 0, 0),
    '8': (0, 1, 1, 0, 0),
    '9': (0, 0, 0, 1, 1),
    ':': (0, 1, 1, 1, 0),
    ';': (0, 1, 1, 1, 1),
    '?': (1, 0, 0, 1, 1),
    '%': (1, 0, 0, 1, 0),
    '$': (1, 0, 1, 0, 0),
    #Switch to Letters
    '<': (1, 1, 1, 1, 1),
    #Switch to Figures
    '>': (1, 1, 0, 1, 1),
    # Carriage Return
    '\n': (0, 0, 0, 1, 0),
    #Line Feed
    #\n': (0, 1, 0, 0, 0),
    '_': (0, 1, 0, 0, 0),
    ' ': (0, 0, 1, 0, 0),
    '-': (1, 1, 0, 0, 0),
    '*': (0, 0, 0, 0, 0),
}


baudot_to_figure = {
    (1, 0, 1, 1, 0): '!',
    (1, 0, 0, 0, 1): '"',
    (0, 0, 1, 0, 1): '#',
    (0, 1, 0, 1, 1): '&',
    (1, 1, 0, 1, 0): "'",
    (1, 1, 1, 1, 0): '(',
    (0, 1, 0, 0, 1): ')',
    (0, 0, 1, 1, 0): ',',
    (0, 0, 1, 1, 1): '.',
    (1, 0, 1, 1, 1): '/',
    (0, 1, 1, 0, 1): '0',
    (1, 1, 1, 0, 1): '1',
    (1, 1, 0, 0, 1): '2',
    (1, 0, 0, 0, 0): '3',
    (0, 1, 0, 1, 0): '4',
    (0, 0, 0, 0, 1): '5',
    (1, 0, 1, 0, 1): '6',
    (1, 1, 1, 0, 0): '7',
    (0, 1, 1, 0, 0): '8',
    (0, 0, 0, 1, 1): '9',
    (0, 1, 1, 1, 0): ':',
    (0, 1, 1, 1, 1): ';',
    (1, 0, 0, 1, 1): '?',
    (1, 0, 0, 1, 0): '%',
    (1, 0, 1, 0, 0): '$',
    #Switch to Letters
    (1, 1, 1, 1, 1): '<',
    #Switch to Figures
    (1, 1, 0, 1, 1): '>',
    # Carriage Return
    (0, 0, 0, 1, 0): '\n',
    #Line Feed
    #'\n': (0, 1, 0, 0, 0),
    (0, 1, 0, 0, 0): '_',
    (0, 0, 1, 0, 0): ' ',
    (1, 1, 0, 0, 0): '-',
    (0, 0, 0, 0, 0): '*',
}


SWITCH_TO_LETTERS_BAUDOT = (1, 1, 1, 1, 1)
SWITCH_TO_FIGURES_BAUDOT = (1, 1, 0, 1, 1)

# Exceptions are only used when bruteforcing
def char_to_baudot(char, using_letters, using_figures):
    if using_letters:
        try:
            return letter_to_baudot[char.upper()]
        ## If the letter does not have a baudot code
        ## (it is '*')
        except KeyError:
            return (0, 0, 0, 0, 0)
    elif using_figures:
        try:
            return figures_to_baudot[char]
        ## If the figure does not have a baudot code
        ## (it is '*')
        except KeyError:
            return (0, 0, 0, 0, 0)


def baudot_to_char(code, using_letters, using_figures):
    if using_letters:
        try:
            return baudot_to_letter[code]
        # If the baudot code does not have a letter
        except KeyError:
            return '*'
    elif using_figures:
        try:
            return baudot_to_figure[code]
        # If the baudot code does not have a figure
        except KeyError:
            return '*'


def string_to_baudot(text):
    """Converts a string into a list of Baudot codes"""
    using_figures = False
    using_letters = True
    baudot_list = []
    for letter in text:
        if using_figures and letter.upper() in LETTERS:
            using_figures = False
            using_letters = True
            #print SWITCH_TO_LETTERS_BAUDOT
            baudot_list.append(SWITCH_TO_LETTERS_BAUDOT)
        elif using_letters and letter in FIGURES:
            using_letters = False
            using_figures = True
            #print SWITCH_TO_FIGURES_BAUDOT
            baudot_list.append(SWITCH_TO_FIGURES_BAUDOT)
        #TODO: use constance for '<'
        if using_figures and letter == '<':
            using_figures = False
            using_letters = True
        elif using_letters and letter == '>':
            using_letters = False
            using_figures = True
        #print letter, char_to_baudot(letter, using_letters, using_figures)
        baudot_list.append(char_to_baudot(letter, using_letters, using_figures))

    return baudot_list


def baudot_to_string(baudot_list):
    """Converts a list of Baudot codes into a string"""
    using_figures = True
    using_letters = True
    plaintext = []
    for code in baudot_list:
        if using_figures and code == SWITCH_TO_LETTERS_BAUDOT:
            using_figures = False
            using_letters = True
        elif using_letters and code == SWITCH_TO_FIGURES_BAUDOT:
            using_letters = False
            using_figures = True
        #print code, baudot_to_char(code, using_letters, using_figures)
        #if using_letters and using_figures and baudot_to_char(code, True, False) in LETTERS:
        #    using_letters = True
        #    using_figures = False
        #    #print baudot_to_char(SWITCH_TO_LETTERS_BAUDOT, using_letters, using_figures)
        #    plaintext.append(baudot_to_char(SWITCH_TO_LETTERS_BAUDOT, using_letters, using_figures))
        plaintext.append(baudot_to_char(code, using_letters, using_figures))
        #print baudot_to_char(code, using_letters, using_figures)
    return plaintext


def rotate_wheel(wheel):
    # set new_wheel to a set of the second element to the end
    new_wheel = wheel[1:]
    # add the first element of wheel to the end
    new_wheel.append(wheel[0])
    return new_wheel


def rotate_wheel_by(wheel, times):
    for _ in range(times):
        wheel = rotate_wheel(wheel)
    return wheel


def rotate_wheel_list_by(wheel_list, times):
    new_wheel_list = []
    for wheel in wheel_list:
        new_wheel_list.append(rotate_wheel_by(wheel, times))
    return new_wheel_list


def rotate_wheel_list(wheel_list):
    return rotate_wheel_list_by(wheel_list, 1)


def rotate_first_wheel_in_list(wheel_list):
    wheel_list[0] = rotate_wheel_by(wheel_list[0], 1)
    return wheel_list


def rotate_second_wheel_in_list(wheel_list):
    wheel_list[1] = rotate_wheel_by(wheel_list[1], 1)
    return wheel_list


def wheel_list_encrypt(baudot_code, wheel_list):
    encrypted_code = []
    for i, wheel in zip(baudot_code, wheel_list):
        encrypted_code.append(xor(i, wheel[0]))
    return tuple(encrypted_code)


def do_lorenz(plaintext, chi_wheels, mu_wheels, psi_wheels):
    cypher_text = []
    for code in plaintext:
        temp_code = wheel_list_encrypt(code, chi_wheels)
        cypher_code = wheel_list_encrypt(temp_code, psi_wheels)

        chi_wheels = rotate_wheel_list(chi_wheels)

        if mu_wheels[0][0] == 1:
            mu_wheels = rotate_second_wheel_in_list(mu_wheels)
        mu_wheels = rotate_first_wheel_in_list(mu_wheels)

        if mu_wheels[1][0] == 1:
            psi_wheels = rotate_wheel_list(psi_wheels)

        cypher_text.append(cypher_code)

    return cypher_text


def set_wheel_list_positions(wheel_list, wheel_positions):
    new_wheel_list = []
    for wheel, position in zip(wheel_list, wheel_positions):
        new_wheel = rotate_wheel_by(wheel, position)
        new_wheel_list.append(new_wheel)
    return new_wheel_list


def lorenz(text, chi_positions, mu_positions, psi_positions):
    chi_wheels = [
        #41
        [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        #31
        [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1],
        #29
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1],
        #26
        [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0],
        #23
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0],
    ]
    mu_wheels = [
        #61
        [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        #37
        [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0]
    ]
    psi_wheels = [
        #43
        [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0],
        #47
        [0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1],
        #51
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        #53
        [0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1],
        #59
        [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1]
    ]
    # Set the wheels to their starting position
    chi_wheels = set_wheel_list_positions(chi_wheels, chi_positions)
    mu_wheels = set_wheel_list_positions(mu_wheels, mu_positions)
    psi_wheels = set_wheel_list_positions(psi_wheels, psi_positions)

    return do_lorenz(text, chi_wheels, mu_wheels, psi_wheels)

