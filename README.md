# Lorenz Cipher

Available at http://lorenz-cipher.herokuapp.com/

To Run Locally
Using Python 2.7
Install the dependencies in requirements.txt
$ pip install -r requirements.txt

Start Web Server
python start_web_server.py

Navigate to http://localhost:5000/
